x=90
y=10

echo "Integer Calculations x=" $x "; y=" $y

echo "x + y =" $((x + y))
echo "x - y =" $((x - y))
echo "x * y =" $((x * y))
echo "x / y =" $((x / y))
echo "x % y =" $((x % y))

echo "String manipulation"

hello="Hello "
echo $hello

hello=$hello"World"
echo $hello

sub=${hello:3:4}
echo $sub

expr length "$hello"
