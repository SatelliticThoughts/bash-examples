running=true
num=0

while $running
do
	if ((num > 5));
	then
		echo "Exiting while loop"
		break
	fi
	num+=1
done

for i in {0..12}
do
	echo "10 * $i =" $((10*i))
done

words="blah test something else"
for word in $words
do
	echo $word
done
