if true;
then
	echo "This will print"
fi

if false;
then
	echo "This won't print"
else
	echo "This will print"
fi

if false;
then
	echo "This won't print"
elif true;
then
	echo "This will print"
else
	echo "This won't print"
fi

if true && true;
then
	echo "This will print"
fi

if true && false;
then
	echo "This won't print"
fi

if false && false;
then
	echo "This won't print"
fi

if true || true;
then
	echo "This will print"
fi

if true || false;
then
	echo "This will print"
fi

if false || false;
then
	echo "This won't print"
fi

x=10
y=8

if ((x == y));
then
	echo "X equals y"
fi

if ((x != y));
then
	echo "x doesn't equal y"
fi

if ((x > y));
then
	echo "x greater than y"
fi

if ((x < y));
then
	echo "x less than y"
fi

