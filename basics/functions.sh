function sayHello() {
	echo "Hello"
}

function doubleNumber() {
	echo $(($1 * 2))
}

function addNumbers() {
	return $(($1 + $2))
}

sayHello

doubleNumber 45

addNumbers 57 25
echo $?
