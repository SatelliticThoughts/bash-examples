# bash-examples

bash examples of various things.

## Set Up
After cloning or downloading, files are run with bash.

```
bash "filename.sh" # run file
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
